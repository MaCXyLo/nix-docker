{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  shortName = "19.09";
  name = "nixos-19.09.1320.4ad6f1404a8";
  version = "2019-11-23";

  src = fetchurl {
    url = "https://releases.nixos.org/nixos/${shortName}/${name}/nixexprs.tar.xz";
    sha256 = "e4305fad8296a3b201150e98daebc69626022dc0d44d9d09ba324c2b7b17e1f3";
  };

  dontBuild = true;
  preferLocalBuild = true;

  installPhase = ''
    cp -a . $out
  '';
}
